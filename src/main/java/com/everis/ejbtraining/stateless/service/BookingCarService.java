package com.everis.ejbtraining.stateless.service;

import com.everis.ejbtraining.stateless.interceptor.SecurityInterceptor;
import com.everis.ejbtraining.stateless.model.BookingCar;
import com.everis.ejbtraining.stateless.model.Car;

import javax.ejb.Local;
import javax.interceptor.Interceptors;
import java.util.List;

/**
 * Created by hgranjal on 26/06/2018.
 */
@Local
public interface BookingCarService {
    List<Car> findCarAll(String username);
    List<BookingCar> findBookingCarAll(String username);
    BookingCar book(String username, Car car, String user);
    void remove(String username, BookingCar bookingCar);
}
