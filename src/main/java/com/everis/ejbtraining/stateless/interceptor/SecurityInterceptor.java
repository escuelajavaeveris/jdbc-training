package com.everis.ejbtraining.stateless.interceptor;

import javax.annotation.PostConstruct;
import javax.ejb.EJBAccessException;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hgranjal on 26/06/2018.
 */
@Interceptor
public class SecurityInterceptor {

    private List<String> usersWithPermission = new ArrayList<>();

    public SecurityInterceptor() {
        usersWithPermission.add("Henrique");
        usersWithPermission.add("Marina");
    }

//    @PostConstruct
//    public void init() {
//        usersWithPermission.add("Henrique");
//        usersWithPermission.add("Marina");
//    }

    @AroundInvoke
    public Object checkAccess(InvocationContext ctx) throws Exception {
        Object[] params = ctx.getParameters();
        String userName = (String) params[0];
        boolean userHasPermission = usersWithPermission.contains(userName);

        if (userHasPermission) {
            return ctx.proceed();
        } else {
            throw new EJBAccessException("The user doesn't have permissions!");
        }
    }
}
