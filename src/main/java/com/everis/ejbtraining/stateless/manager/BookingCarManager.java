package com.everis.ejbtraining.stateless.manager;

import com.everis.ejbtraining.stateless.model.BookingCar;
import com.everis.ejbtraining.stateless.model.Car;
import com.everis.ejbtraining.stateless.store.BookingCarStore;
import com.everis.ejbtraining.stateless.store.Connection;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by hgranjal on 26/06/2018.
 */
@ApplicationScoped
public class BookingCarManager {

    @Inject
    private BookingCarCache bookingCarCache;
    @Inject
    private BookingCarStore bookingCarStore;

    public List<Car> findCarAll(Connection connection) {
        List<Car> carList;
        if (bookingCarCache.getCarList().isEmpty()) {
            carList = bookingCarStore.findCarAll(connection);
            bookingCarCache.setCarList(carList);
        } else {
            carList = bookingCarCache.getCarList();
        }
        return carList;
    }

    public List<BookingCar> findBookingCarAll(Connection connection) {
        List<BookingCar> bookingCarList;
        if (bookingCarCache.getBookingCarList().isEmpty()) {
            bookingCarList = bookingCarStore.findBookingCarAll(connection);
            bookingCarCache.setBookingCarList(bookingCarList);
        } else {
            bookingCarList = bookingCarCache.getBookingCarList();
        }
        return bookingCarList;
    }

    public void remove(BookingCar bookingCar, Connection connection) {
        bookingCarStore.remove(bookingCar, connection);
        //TODO: remove car from cache?
    }

    public BookingCar book(Car car, String user, Connection connection) {
        return bookingCarStore.book(car, user, connection);
        //TODO: add bookingCar in cache?
    }
}
