package com.everis.ejbtraining.stateless.manager;

import com.everis.ejbtraining.stateless.model.Car;
import com.everis.ejbtraining.stateless.store.Connection;
import com.everis.ejbtraining.stateless.store.ProviderCarStore;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Created by hgranjal on 26/06/2018.
 */
@ApplicationScoped
public class ProviderCarManager {

    @Inject
    private ProviderCarStore providerCarStore;

    public Car buyCar(Car car, Connection connection) {
        return providerCarStore.buyCar(car, connection);
        //TODO: add car in cache?
    }
}
